#pragma once
#include "GenericFile.h"
#include "BinaryFile.h"

class TextFile :
	public GenericFile
{
public:
	int lineCount;

	TextFile(void);
	TextFile(string _path);
	//konstruktor konwerterujacy, dzieki temu ze przyjmuje wskaznik do GenericFile
	//moze rownierz przyjac wskaznik do jego przodka.
	TextFile(GenericFile* file); 
	~TextFile(void);

	void GetFileInfo();
	void Print();

	void GetLinesCount();
};

