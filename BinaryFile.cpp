#include "StdAfx.h"
#include "BinaryFile.h"

//obydwa konstruktory wykonuja najpierw konstruktor klasy bazowej
BinaryFile::BinaryFile(string _path) 
	: GenericFile(_path){}
BinaryFile::BinaryFile(string _path, string hash, int size) 
	: GenericFile(_path, hash, size){}
BinaryFile::~BinaryFile(void){}

void BinaryFile::Print()
{
	PrintBasicInfo();
}

void BinaryFile::GetFileInfo()
{
	GenerateSHA1Hash();
	GetFileSize();
}
