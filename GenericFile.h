#pragma once
#include<string>
using namespace std;

class GenericFile
{
public:
	string path;
	string fileName;
	string hashSHA1;
	string ext;
	int size;
	
	GenericFile(void);
	GenericFile(string _path);
	GenericFile(string _path, string hash, int size);
	~GenericFile(void);

	string GenerateSHA1Hash(void);
	void GetFileSize();
	void GetFileNameAndExt();
	void PrintBasicInfo();

	//funkcje wirtualne dla ktorych wymagana 
	//jest osobna impementacja dla kazdego potomka
	virtual void Print() = 0;
	virtual void GetFileInfo() = 0;
};

