#pragma once
#include "GenericFile.h"
#include "BinaryFile.h"
#include "TextFile.h"
#include "sqlite\sqlite3.h"
#include <vector>
#include <string>
using namespace std;

class DB
{
public:
	sqlite3 *database;

	DB(void);
	~DB(void);

	int SaveFile(GenericFile *genericFile);
	int SaveFiles(vector<GenericFile*> fileList);

	vector<GenericFile*> GetFileData(string queryHash);
};

