#include "StdAfx.h"
#include "GenericFile.h"
#include <iostream>
using namespace std;

GenericFile::GenericFile(void)
{
}

GenericFile::GenericFile(string _path)
{
	path = _path;
	GetFileNameAndExt();
}

GenericFile::GenericFile(string _path, string _hash, int _size)
{
	path = _path;
	hashSHA1 = _hash;
	size = _size;
	GetFileNameAndExt();
}

GenericFile::~GenericFile(void)
{
}

string GenericFile::GenerateSHA1Hash(void)
{
	hashwrapper *wrapper = new md5wrapper();
	hashSHA1 = wrapper->getHashFromFile(path);
	return hashSHA1;
}

void GenericFile::GetFileSize()
{
	FILE *filePtr;
	filePtr = fopen(path.c_str(), "r");
	fseek(filePtr, 0, SEEK_END);
	size = ftell(filePtr);
	fclose(filePtr);
}

void GenericFile::GetFileNameAndExt()
{
	int pos = path.rfind("/");
	int len = path.size() - pos;
	fileName = path.substr(pos + 1, len);

	pos = fileName.rfind(".");
	if (pos != string::npos)
	{
		len = fileName.size() - pos;
		ext = fileName.substr(pos + 1, len);
	}
	else
	{
		ext = "";
	}
}

void GenericFile::PrintBasicInfo()
{
	cout << hashSHA1.substr(0, 8) << "\t";
	cout << size << "\t";
	cout << ext << "\t";
	cout << fileName << "\t";
	cout << endl;
}
