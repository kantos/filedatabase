#include "StdAfx.h"
#include "DB.h"
#include <iostream>

DB::DB(void)
{
	sqlite3_open("db.db", &database);
}

DB::~DB(void)
{
	sqlite3_close(database); //zamykam polaczenie z baza
	delete database; //usuwam wskaznik do obiektu sqlite3
}

int DB::SaveFiles(vector<GenericFile*> fileList)
{
	int i;
	int count = 0;
	const char* query;
	sqlite3_stmt *statement;
	
	query = "INSERT INTO file (sha1, path, size) VALUES (?, ?, ?);";
	sqlite3_prepare_v2(database, query, -1, &statement, 0);
	sqlite3_exec(database, "BEGIN", 0, 0, 0); //rozpoczecie transakcji

	//w tej petli przypisuje odpoiwednie parametry do zapytania
	for(i = 0; i<fileList.size(); i++)
	{
		sqlite3_bind_text(statement, 1, fileList[i]->hashSHA1.c_str(), -1, SQLITE_STATIC);
		sqlite3_bind_text(statement, 2, fileList[i]->path.c_str(), -1, SQLITE_STATIC);
		sqlite3_bind_int(statement, 3, fileList[i]->size);
		sqlite3_step(statement);
		sqlite3_reset(statement);
		count++;
	}

	sqlite3_exec(database, "COMMIT", 0, 0, 0); //zakonczenie transakcji
	sqlite3_finalize(statement);
	return count;
}

int DB::SaveFile(GenericFile *genericFile)
{
	vector<GenericFile*> fileList;
	fileList.push_back(genericFile);
	return SaveFiles(fileList);
}

vector<GenericFile*> DB::GetFileData(string queryHash)
{
	sqlite3_stmt *statement;
	GenericFile *file;
	vector<GenericFile*> fileList;
	int stepResult, size;
	string path, hash;
	const char *query;

	query = "SELECT path, sha1, size FROM file WHERE sha1 LIKE '%'||?||'%';";
	sqlite3_prepare_v2(database, query, -1, &statement, 0);
	sqlite3_bind_text(statement, 1, queryHash.c_str(), -1, SQLITE_STATIC);

	while(1)
	{
		stepResult = sqlite3_step(statement);

		if (stepResult != SQLITE_ROW) break;

		//konwersja wskaznika do const char na wskaznik do string
		path = string(reinterpret_cast<const char*>(sqlite3_column_text(statement, 0)));
		hash = string(reinterpret_cast<const char*>(sqlite3_column_text(statement, 1)));
		size = sqlite3_column_int(statement, 2);
		
		file = new BinaryFile(path, hash, size); 
		//jesli plik ma rozszerzenie txt, konwerterujuje go
		//na obiekt typu TextFile
		if (file->ext == "txt")
		{
			file = new TextFile(file);
		}
		fileList.push_back(file);
	}
	sqlite3_finalize(statement);
	return fileList;
}
