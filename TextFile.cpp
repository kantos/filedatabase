#include "StdAfx.h"
#include "TextFile.h"
#include <iostream>

TextFile::TextFile(void){}
TextFile::TextFile(string _path) 
	: GenericFile(_path){}

TextFile::TextFile(GenericFile* file)
{
	this->lineCount = 0;
	this->ext = file->ext;
	this->fileName = file->fileName;
	this->hashSHA1 = file->hashSHA1;
	this->path = file->path;
	this->size = file->size;
	GetLinesCount();
}

TextFile::~TextFile(void){}

void TextFile::GetFileInfo()
{
	GenerateSHA1Hash();
	GetFileSize();
	GetLinesCount();
}

void TextFile::GetLinesCount()
{
	this->lineCount = 0;
	string line;
	ifstream myfile(this->path);

	while (getline(myfile, line))
		++this->lineCount;
}

void TextFile::Print()
{
	PrintBasicInfo();
	cout << " Linie: " << this->lineCount << "\t";
	cout << endl;
}
