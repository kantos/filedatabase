#include "stdafx.h"
#include "GenericFile.h"
#include "DirParser.h"
#include "DB.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int i;
	string command, help, header;

	header = "[SHA1]\t\t[B]\t[typ]\t[nazwa]\n";
	help = "s [sciezka] - skanuj katalog\n"
		"p [fragment hasha] - szukanie plikow po hashu\n"
		"q - wyjscie\n"
		"? - wyswietla te informacje\n";
	
	cout << help;

	DB db = DB();
	vector<GenericFile*> fileList;
	while(1) //glowna petla programu
	{
		cout << "> ";

		getline(cin, command);

		if (command.substr(0, 2) == "s ")
		{
			//skanowanie katalogow
			fileList = DirParser::GetFileList(command.substr(2, command.length()-1));

			cout << header;
			for(i=0; i<fileList.size(); i++)
			{
				fileList[i]->Print();
			}

			db.SaveFiles(fileList);
		}
		else if (command.substr(0, 2) == "p ")
		{
			//przeszukiwanie bazy
			fileList = db.GetFileData(command.substr(2, command.length()-1));
			
			cout << header;
			for(i=0; i<fileList.size(); i++)
			{
				fileList[i]->Print();
			}
		}
		else if (command.substr(0, 1) == "?")
		{
			//pomoc
			cout << help;
		}
		else if (command.substr(0, 1) == "q")
		{
			//wyjscie
			return 0;
		}
		else
		{
			//blad
			cout << "Bledna komenda, wpisz ? by wyswietlic pomoc\n";
		}
	}
}

