#pragma once
#include "GenericFile.h"

class BinaryFile :
	public GenericFile
{
public:
	BinaryFile(void);
	BinaryFile(string _path);

	//ten konstruktor pozwala na spreparowanie dowolnego pliku
	BinaryFile(string _path, string hash, int size); 
	
	~BinaryFile(void);

	void Print();
	void GetFileInfo();
};

