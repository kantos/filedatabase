#pragma once
#include "StdAfx.h"
#include "GenericFile.h"
#include "TextFile.h"
#include "BinaryFile.h"
#include "hashlib\hl_exception.h"
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class DirParser
{
	//w takiej formie jak teraz wydaje sie niepotrzbne umieszczanie tej metody w klasie
	//jednak jesli w przyszlosci bedziemy chcieli dodac inne funkcjonalnosci jak np
	//zwracanie listy katalogow lub dzialania na zwroconych wynikach to zaoszczedzimy
	//troche czasu, ponadto kod bedzie lepiej zorganizowany
public:
	static vector<GenericFile*> GetFileList(string path)
	{
		vector<GenericFile*> fileList;

		DIR *dir;
		struct dirent *ent;
		GenericFile *file;

		if ((dir = opendir(path.c_str())) != NULL) {
			while ((ent = readdir (dir)) != NULL) {
				if (ent->d_type == DT_REG)
				{
					file = new BinaryFile(path + (string)ent->d_name);

					//blok wychwytywania bledu pozwala na nieprzerwane dzialanie programu
					//w przypadku proby przetworzenia nieistniejacego pliku
					try
					{
						file->GetFileInfo();
					}
					catch (hlException e)
					{
						cout << e.error_message() << endl;
						continue; //przerwanie aktualnego przebiegu petli i przejscie do kolejnego
					}

					if (file->ext == "txt")
					{
						file = new TextFile(file);
					}
					fileList.push_back(file);

				}
			}
			closedir (dir);
		} else {
			perror("could not open directory");
		}
		return fileList;
	}
};

